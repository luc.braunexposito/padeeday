# Padeeday

[![Codemagic build status](https://api.codemagic.io/apps/61cedd69eec1c0a0246efa49/61cedd69eec1c0a0246efa48/status_badge.svg)](https://codemagic.io/apps/61cedd69eec1c0a0246efa49/61cedd69eec1c0a0246efa48/latest_build)

## Instructions de build:

### - Android
1. Récupérer l'APK de l'application sur codemagic (via le lien: https://codemagic.io/app/61cedd69eec1c0a0246efa49/build/61db119ed70044d2b7b435fe)
2. * Si vous utilisez un émulateur:  
	&nbsp;&nbsp;&nbsp;&nbsp;I. Glissez l'APK sur l'écran de votre émulateur et l'application s'installera vous n'aurez ensuite plus qu'à lancer directement l'application.
	* Si vous utilisez un téléphone:  
	&nbsp;&nbsp;&nbsp;&nbsp;I. Allez dans les paramètres du téléphone, puis Sécurité.  
	&nbsp;&nbsp;&nbsp;&nbsp;II. Activez les sources inconnues.  
	&nbsp;&nbsp;&nbsp;&nbsp;III. Recherchez l’APK sur votre téléphone (à l’aide d’un explorateur de fichiers par exemple).  
	&nbsp;&nbsp;&nbsp;&nbsp;IV. Lancez le fichier APK et suivez les instructions.  

### - IOS
1. ???



## Fonctionnalitées de l'application

L'application est un exemple de système d'authentification simple avec nom d'utilisateur, mot de passe et photo de profil.  
Les fonctionnalitées de Padeeday sont: Création de compte, connexion, déconnexion et prise de photo.

## Architecture du projet

Avec l'utilisation de flutter nous pouvons utiliser un seul code pour toutes les plateformes.
De ce fait l'architecture du projet est extrêmement simple:
- Un dossier "backend" contenant le serveur utilisé avec l'application (host sur heroku, il n'est donc pas nécessaire de le build en local).
- Un dossier "padiday" contenant l'application flutter. Le code est dans le dossier padiday/lib/ et est découpé en 3 parties:
	* main.dart, point d'entrée de l'application
	* pages/, dossier contenant les différentes pages de l'application
	* controller/, dossier contenant les différents controllers de l'application (pour ce projet il n'y a qu'un controller gérant l'etat global de l'application)

## Librairies utilisées

Librairie 		| Description
------------- 	| -------------
cupertino_icons | Permet d'avoir les icones et polices d'écritures ios.  
http  			| Permet d'effectuer des requêtes dans l'application.   
image_picker 	| Permet de sélectionner une ou plusieurs images de la gallerie de l'appareil ou via la caméra.  
async 			| Permet de réaliser des opérations de manière asynchrone.
jwt_decode 		| Permet de décoder des JSON Web Token.
get				| Module gérant l'état global de l'application
