require("dotenv").config();
require("./config/database").connect();
const express = require("express");

const app = express();
app.use(express.json());
require("./routes/UserManager")(app);
require("./routes/albumManager")(app);
require("./routes/photoManager")(app);
module.exports = app;