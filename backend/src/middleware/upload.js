const multer = require("multer");

//multer options
const upload = multer({
	limits: {
		fileSize: 10000000,
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(png|jpg|jpeg)$/)) {
			cb(new Error('Please upload an image.'))
		}
		cb(undefined, true)
	}
})

module.exports = upload;