const mongoose = require("mongoose");

const photoSchema = new mongoose.Schema({
	title: { type: String},
	location: {type: String},
	photo: {type: Buffer}
});
	
module.exports = mongoose.model("photo", photoSchema);