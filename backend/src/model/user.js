const mongoose = require("mongoose");
const Album = require('./album');

const userSchema = new mongoose.Schema({
	name: { type: String, unique: true },
	password: { type: String },
	token: { type: String },
	profile_pic: { type: Buffer },
	album_list: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'album'
	}]
});

module.exports = mongoose.model("user", userSchema);