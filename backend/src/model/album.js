const mongoose = require("mongoose");
const User = require('./user');
const Photo = require('./photo');

const albumSchema = new mongoose.Schema({
	author: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
	title: { type: String },
	photo_list: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'photo'
	}]
});

module.exports = mongoose.model("album", albumSchema);