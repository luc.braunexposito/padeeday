const auth = require("../middleware/auth");
const Album = require('../model/album');
const User = require("../model/user");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const config = process.env;

module.exports = function (app) {
	// create an album
	app.post('/album/create', auth, async (req, res) => {
		const { title } = req.body;
		if (!(title)) {
			res.status(400).send("All input is required");
		}
		const token = req.body.token || req.query.token || req.headers["x-access-token"];
		jwt.verify(token, config.TOKEN_KEY, async (err, verifiedJwt) => {
			if (err) {
				res.status(400).send(err.message);
			} else {
				const doc = new Album({
					author: mongoose.Types.ObjectId(verifiedJwt.user_id),
					title: title,
					photo_list: []
				});
				await doc.save();
				const user = await User.findOne({ _id: mongoose.Types.ObjectId(verifiedJwt.user_id) });
				user.album_list.push(mongoose.Types.ObjectId(doc._id));
				user.save();
				res.status(200).send(doc._id);
			}
		});
	});

	// list all albums of logged user
	app.get('/album/list', auth, async (req, res) => {
		const token = req.body.token || req.query.token || req.headers["x-access-token"];
		jwt.verify(token, config.TOKEN_KEY, async (err, verifiedJwt) => {
			if (err) {
				res.send(err.message);
			} else {
				User.findOne({_id: mongoose.Types.ObjectId(verifiedJwt.user_id)}).populate('album_list').exec(function (err, user) {
					if (err)
						res.status(400).send(err);
					else {
						console.log(user.album_list);
						if (!(user.album_list))
							user.album_list = [];
						res.status(200).send(user.album_list);
					}
				});
			}
		});
	});

	//get specific album (atm j'envoie la premiere photo de l'album sans les info à voir ce dont on a besoin)
	app.get('/album/', auth, async (req, res) => {
		const { albumId } = req.body;
		const token = req.body.token || req.query.token || req.headers["x-access-token"];
		jwt.verify(token, config.TOKEN_KEY, async (err, verifiedJwt) => {
			if (err) {
				res.send(err.message);
			} else {
				Album.findOne({_id: albumId}).populate('photo_list').exec(function (err, album) {
					if (err)
						res.status(400).send(err);
					else {
						//res.set('Content-Type', 'image/png');
						if (!(album.photo_list))
							album.photo_list = [];
						res.status(200).send(album.photo_list);
					}
				});
			}
		});
	});
}