const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/user");
const auth = require("../middleware/auth");
const upload = require("../middleware/upload");
const sharp = require("sharp");

module.exports = function (app) {
	//register
	app.post("/register", upload.single('profile_pic'), async (req, res) => {
		try {
			const { name, password } = req.body;
			const img = req.file.buffer;
			if (!(name && password && img)) {
				res.status(400).send("All input is required");
			}
			const oldUser = await User.findOne({ name });
			if (oldUser) {
				return res.status(409).send("User Already Exist. Please Login");
			}
			encryptedPassword = await bcrypt.hash(password, 10);
			const buffer = await sharp(img).resize({ width: 500, height: 500}).png().toBuffer();
			const user = await User.create({
				name: name,
				password: encryptedPassword,
				profile_pic: buffer,
				album_list: []
			});
			res.status(201).end();
		} catch (err) {
			console.log(err);
		}
	});

	// login
	app.post("/login", async (req, res) => {
		try {
			const { name, password } = req.body;

			if (!(name && password)) {
				res.status(400).send("All input is required");
			}
			const user = await User.findOne({ name });
			if (user && (await bcrypt.compare(password, user.password))) {
				const token = jwt.sign(
					{ user_id: user._id, name },
					process.env.TOKEN_KEY,
					{
						expiresIn: "2h",
					}
				);
				user.token = token;
				res.status(200).json(user.token);
			}
			else {
				res.status(400).send("Invalid Credentials");
			}
		} catch (err) {
			console.log(err);
		}
	});

	//get a profile picture by userId
	app.get('/user/:id/photo', auth, async (req, res) => {
		try {
			const user = await User.findById(req.params.id);
			if (!user || !user.profile_pic) {
				throw new Error();
			}
			res.set('Content-Type', 'image/png');
			res.status(200).send(user.profile_pic);
		} catch (e) {
			res.status(404).send();
		}
	});

	//delete a specific user
	app.delete("/delete/user", async (req, res) => {
		try {
			const { name } = req.body;

			if (!(name)) {
				res.status(400).send("All input is required");
			}
			await User.findOneAndDelete({ name });
			res.status(204).send("deleted");
		} catch (err) {
			console.log(err);
		}
	});

	//list all users, need to be authenticate
	app.get("/users", auth, async (req, res) => {
		User.find({}, function(err, users) {
			res.status(200).send({"users": users});
		 });
	});
	
}