const Album = require('../model/album');
const Photo = require('../model/photo');
const upload = require("../middleware/upload");
const auth = require("../middleware/auth");
const sharp = require("sharp");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
var fetch = require('node-fetch');

const config = process.env;

module.exports = function (app) {
	app.post('/photo/add', [auth, upload.single("photo")], async (req, res) => {
		const { title, albumId, city } = req.body;
		const img = req.file.buffer;
		if (!(title && img && albumId && city)) {
			res.status(400).end();
		}
		const token = req.body.token || req.query.token || req.headers["x-access-token"];
		jwt.verify(token, config.TOKEN_KEY, async (err, verifiedJwt) => {
			if (err) {
				res.send(err.message);
			} else {
				const buffer = await sharp(img).png().toBuffer();
				const doc = new Photo({
					title: title,
					location: city,
					photo: buffer
				});
				await doc.save();
				const album = await Album.findOne({ _id: mongoose.Types.ObjectId(albumId) });
				album.photo_list.push(mongoose.Types.ObjectId(doc._id));
				album.save();
				res.status(200).send(verifiedJwt.userId);
			}
		});
	});

}