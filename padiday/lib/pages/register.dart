// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:async/async.dart';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:image_picker/image_picker.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late TextEditingController _name;
  late TextEditingController _password;
  final GlobalKey<FormState> _formKey = GlobalKey();
  List<XFile>? _imageFileList;
  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _name = TextEditingController();
    _password = TextEditingController();
  }

  @override
  void dispose() {
    _name.dispose();
    _password.dispose();
    maxWidthController.dispose();
    maxHeightController.dispose();
    qualityController.dispose();
    super.dispose();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  set _imageFile(XFile? value) {
    _imageFileList = value == null ? null : [value];
  }

  void _onImageButtonPressed(ImageSource source,
      {BuildContext? context}) async {
    final XFile? pickedFile = await _picker.pickImage(
      source: source,
      maxHeight: 480,
      maxWidth: 640,
      imageQuality: 50,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
        _imageFileList = response.files;
      });
    }
  }

  Future<void> creacteAcc() async {
    String urlStr = "https://serverflutter.herokuapp.com/register";
    var url = Uri.parse(urlStr);
    var stream = http.ByteStream(
        DelegatingStream.typed(File(_imageFileList![0].path).openRead()));
    var length = await File(_imageFileList![0].path).length();
    var request = http.MultipartRequest("POST", url);
    var multipartFile = http.MultipartFile('profile_pic', stream, length,
        filename: path.basename(File(_imageFileList![0].path).path));
    request.fields["name"] = _name.text;
    request.fields["password"] = _password.text;

    request.files.add(multipartFile);

    await request.send().then((result) async {
      if (result.statusCode == 201) {
        Navigator.pushNamed(context, "/login");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        showModalBottomSheet<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: 200,
                                color: Colors.amber,
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      ElevatedButton(
                                        child: const Text(
                                            'Sélectionner une photo dans la gallerie'),
                                        onPressed: () => _onImageButtonPressed(
                                            ImageSource.gallery,
                                            context: context),
                                      ),
                                      ElevatedButton(
                                        child: const Text('Prendre une photo'),
                                        onPressed: () => _onImageButtonPressed(
                                            ImageSource.camera,
                                            context: context),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            });
                      },
                      child: CircleAvatar(
                        radius: 55.0,
                        backgroundColor: Colors.teal,
                        child: _imageFileList != null
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.file(
                                  File(_imageFileList![0].path),
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50.0),
                                  color: Colors.grey[200],
                                ),
                                width: 100,
                                height: 100,
                                child: Icon(
                                  Icons.camera_alt,
                                  color: Colors.grey[800],
                                ),
                              ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: TextFormField(
                      controller: _name,
                      decoration: InputDecoration(
                        labelText: 'Nom',
                        labelStyle: const TextStyle(
                          color: Colors.black,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 3, color: Colors.black),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 3, color: Colors.black),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: TextFormField(
                      controller: _password,
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Mot de passe',
                        labelStyle: const TextStyle(
                          color: Colors.black,
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 3, color: Colors.black),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 3, color: Colors.black),
                          borderRadius: BorderRadius.circular(15),
                        ),
                      ),
                    ),
                  ),
                  ElevatedButton(
                    child: const Text('Créer son compte'),
                    onPressed: () {
                      creacteAcc();
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
