import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'package:padiday/controller/stateController.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ImageProvider? _profilePic;
  final _controller = Get.put(Controller());

  Future<void> getUserInfo() async {
    String? urlStr = "https://serverflutter.herokuapp.com/user/" +
        _controller.payload['user_id']! +
        "/photo";
    Uri tmp = Uri.parse(urlStr);
    final newURI =
        tmp.replace(queryParameters: {"token": _controller.token.string});
    final response = await http.get(newURI, headers: {
      "Accept": "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
    });
    if (response.statusCode == 200) {
      setState(() {
        _profilePic = Image.memory(response.bodyBytes).image;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getUserInfo();
  }

  void dipose() {
    getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
      backgroundColor: Colors.teal,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              radius: 55.0,
              backgroundColor: Colors.black,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: _profilePic != null
                    ? Image(
                        image: _profilePic!,
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      )
                    : null,
              ),
            ),
            ElevatedButton(
              child: const Text('Se déconnecter'),
              onPressed: () {
                _controller.disconnect();
                Navigator.pushNamed(context, "/login");
              },
            ),
          ],
        ),
      ),
    );
  }
}
