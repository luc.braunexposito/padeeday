import 'package:get/get.dart';
import 'package:jwt_decode/jwt_decode.dart';

class Controller extends GetxController {
  Map<String?, dynamic> payload = <String?, dynamic>{}.obs;
  RxString token = "".obs;

  connect(String tkn) {
    token = RxString(tkn);
    payload = Jwt.parseJwt(token.string);
    update();
  }

  disconnect() {
    token = "".obs;
    payload = <String?, dynamic>{}.obs;
    update();
  }
}
