import 'package:flutter/material.dart';
import 'pages/home.dart' as home_page;
import 'pages/login.dart' as login_page;
import 'pages/register.dart' as register_page;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.teal,
        ),
        initialRoute: "/login",
        routes: {
          "/": (context) => const home_page.HomePage(
                title: '',
              ),
          "/login": (context) => const login_page.LoginPage(),
          "/register": (context) => const register_page.RegisterPage(),
        });
  }
}
